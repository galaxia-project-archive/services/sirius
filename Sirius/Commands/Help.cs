﻿using Discord;
using Discord.Addons.CommandsExtension;
using Discord.Commands;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.Commands
{
    [Group("help")]
    [Name(":bookmark: Help")]
    [Summary("help menu")]
    public class HelpModule : ModuleBase<SocketCommandContext>
    {
        private readonly CommandService Commands;
        private readonly IConfigurationRoot Config;

        public HelpModule(CommandService service, IConfigurationRoot config)
        {
            Commands = service;
            Config = config;
        }

        [Command("")]
        [Summary("Shows help menu.")]
        public async Task Help([Remainder] string command = null)
        {
            var botPrefix = Config["prefix"];
            var helpEmbed = Commands.GetDefaultHelpEmbed(command, botPrefix);
            await Context.Channel.SendMessageAsync(embed: helpEmbed);
        }
    }
}
