﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sirius.Services
{
    class Log
    {
        private readonly ILogger _logger;
        public Log(DiscordSocketClient client, ILogger logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            client.Log += ClientLogCallback;
        }

        public Task RegisterCommand(CommandService command)
        {
            command.Log += ClientLogCallback;
            return Task.CompletedTask;
        }

        public Task RemoveCommand(CommandService command)
        {
            command.Log -= ClientLogCallback;
            return Task.CompletedTask;
        }

        private Task ClientLogCallback(LogMessage logMessage)
        {
            switch (logMessage.Severity)
            {
                case LogSeverity.Critical:
                    _logger.Fatal(logMessage.Message);
                    break;
                case LogSeverity.Error:
                    _logger.Error(logMessage.Message);
                    break;
                case LogSeverity.Warning:
                    _logger.Warn(logMessage.Message);
                    break;
                case LogSeverity.Info:
                    _logger.Info(logMessage.Message);
                    break;
                case LogSeverity.Verbose:
                    _logger.Trace(logMessage.Message);
                    break;
                case LogSeverity.Debug:
                    _logger.Debug(logMessage.Message);
                    break;
            }
            return Task.CompletedTask;
        }
    }
}
