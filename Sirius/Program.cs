﻿using System;

using NLog;
using NLog.Targets;

namespace Sirius
{
    class Program
    {
        public static ILogger Logger { get; private set; } = LogManager.GetLogger("ROOT");

        static void Main(string[] args)
        {
            ConsoleTarget logTarget = new ConsoleTarget();
            logTarget.Layout = "${date:format=HH\\:MM\\:ss} ${logger} ${message}";
            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(logTarget, LogLevel.Debug);

            Logger.Info("Application startup.");

            Sirius sirius = new Sirius();
            sirius.Initialize(args).Wait();
            sirius.Run().Wait();
        }
    }
}
