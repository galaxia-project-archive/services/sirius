﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Rpc.Payload
{
    public class Variant
    {
        public string type { get; set; }
        public object value { get; set; }
    }

    public class Variant<T1>
    {
        public string type { get; set; }
        public T1 value { get; set; }
    }

    public class Variant<T1, T2>
    {
        [StructLayout(LayoutKind.Explicit)]
        public struct OneOf
        {
            [FieldOffset(0)]
            public T1 First;

            [FieldOffset(0)]
            public T2 Second;
        }

        public string type { get; set; }
        public OneOf value { get; set; }
    }
}
